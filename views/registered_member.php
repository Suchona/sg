<?php
ob_start();
include('header.php');
include('session.php');
include_once('printscript.php');
//var_dump($profile);
$objAlumni = new \App\Alumni\Alumni();
$_GET['id']='all';
$objAlumni->setData($_GET);
$allmembers=$objAlumni->view();
if(!$singleUser['role']='Admin')
App\Utility\Utility::redirect('profile.php');
//var_dump($allmembers);
?>
<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class=" col-md-4">
				<div class="inner-heading">

				</div>
			</div>
			<div class=" col-md-8">
				<ul style="background-color:inherit;" class="breadcrumb">
					<li><a href="home.php"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>

				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div style="width: auto;" class="container">

		<div id="dvContainer"  class="">
			<style>
				<?php
               //include ('../resource/css/printsetup.css');
              //include ('../resource/css/bootstrap.min.css');
               //include_once('printscript.php');
                ?>
				table {
					border-collapse: collapse;
				}

				table, th, td {
					border: 1px solid black;
				}
			</style>

			<div align="center" class="col-md-12  table-responsive">
				<form class="well form-horizontal" action="#" method="post"  id="contact_form">
					<h2>The Chittagong University English Alumni Association</h2>
					<h3>4th Alumni Reunion-2019</h3>
					<fieldset>
						<legend><center><font size="" class="">(Registered Member List)</font></center></legend>
						<table style="text-align: center; font-size: 13px;" class="table table-bordered" >
							<thead>
							<tr>
								<th style="text-align: center;">ID</th>
								<th style="text-align: center;">Photo</th>
								<th style="text-align: center;">Name</th>
								<th style="text-align: center;">Member Type</th>
								<th style="text-align: center;">Batch</th>
								<th style="text-align: center;">Mobile</th>
								<th style="text-align: center;">Email</th>
								<th style="text-align: center;">Participants</th>
								<th style="text-align: center;">Amount</th>
								<th style="text-align: center;">Payment Status</th>
								<th class="leftprint" style="text-align: center;">Action</th>
							</tr>
							</thead>
							<?php
							$all_participant=0;
							$all_amount=0;
							$paid=0;
							$allalumni=0;
							$allSpouse=0;
							$allGuest=0;
							$allChild=0;
							$allInfant=0;
							$allBaby=0;
							foreach($allmembers as $singlemember){

								###################################Participant Count  */
								$total_participant=1; $allalumni+=$total_participant;

								if($singlemember->spouse>=1500){
									$spouse=1;
									$total_participant=$total_participant+$spouse; $allSpouse+=$spouse;
								}

								if($singlemember->guest>=1500){
									$total_guest=round($singlemember->spouse/1500);
									$total_participant=$total_participant+$total_guest; $allGuest+=$total_guest;
								}

								if($singlemember->child>=1000){
									$total_child=$singlemember->child/1000;
									$total_participant=$total_participant+$total_child; $allChild+=$total_child;
								}
								if($singlemember->infant>=500){
									$total_infant=$singlemember->infant/500;
									$total_participant=$total_participant+$total_infant; $allInfant+=$total_infant;
								}

								if($singlemember->baby==!null){
									$total_baby=1;
									$total_participant=$total_participant+$total_baby; $allBaby+=$total_baby;
								}
								if($singlemember->payment_status=='Paid'){
									$paid=$paid+$singlemember->total;

								}
								$total_amount=$singlemember->self+$singlemember->spouse+$singlemember->guest+$singlemember->child+$singlemember->infant;
								$all_amount=$all_amount+$total_amount;
								$all_participant=$all_participant+$total_participant;
								$unpaid=$all_amount-$paid;
								###################################Participant Count  */

								if($singlemember->membertype==1){$member="Life Member";}else{ $member="General Member";}

								echo "<tr>
                              <td style=\"text-align: center;\">$singlemember->id</td>
                              <td style=\"padding-left: 10px;\">
                                <img  width=\"30px\"  src=\"uploads/$singlemember->photo\" class=\"img img-responsive img-circle\" alt=\"name\">
                              </td>
                              <td style=\"text-align: center;\">$singlemember->full_name</td>
                              <td style=\"text-align: center;\">$member</td>
                              <td style=\"text-align: center;\">$singlemember->batch</td>
                              <td style=\"text-align: center;\">$singlemember->mobile_no</td>
                              <td style=\"text-align: center;\">$singlemember->email</td>
                              <td style=\"text-align: center;\">$total_participant </td>
                              <td style=\"text-align: center;\">$total_amount Tk.</td>
                              <td style=\"text-align: center;\">$singlemember->payment_status</td>
                              <td class=\"leftprint\" style=\"text-align: center;\"><a  color: #000;' target='_blank' href='profile.php?mobile_no=$singlemember->mobile_no&email=$singlemember->email'>View</a> &nbsp; <a href='#'>Edit</a> </td>
                            </tr>";
							}
							echo "<tr>
                              <td colspan='6' style=\"text-align: right;\">Total</td>
                              <td style=\"text-align: center;\">$all_participant </td>
                              <td style=\"text-align: center;\">$all_amount Tk.</td>
                              <td style=\"text-align: center;\">Paid:$paid | Unpaid: $unpaid</td>
                              <td style=\"text-align: center;\"></td>
                              <td class=\"leftprint\" style=\"text-align: center;\"></td>
                            </tr>";
							?>
						</table>
						<?php echo "Alumni: ".$allalumni." Spouse:".$allSpouse." Guest:".$allGuest." Child:".$allChild." Infant:".$allInfant." Baby:".$allBaby; ?>
			</div>
			</fieldset>
			</form>
		</div>
		<center><button type="button" id="btnPrint" value="Print Div Contents" class="btn btn-dark">Print</button></center>
		<br>
	</div>
	</div>
	</div>
</section>

<?php
include('footer.php');

?>
