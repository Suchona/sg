<?php
	include('header.php');
?>


	<div class="">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="myregarea table-responsive">
						<form class="form-horizontal" action="store.php" method="post"  id="contact_form" enctype="multipart/form-data" >
						<fieldset>
							<legend><center><h3 class="">Registration</h3></center></legend>
							<!--
							<p align="center">(<a href="life_registration.php">Click here to become a Life Member</a>)</p>
							-->
							<div class="text-danger text-center">Before registration please pay the subscription amount. <a href="#nop">Click here to caclculate your total amount.</a> </div>
							<table class="table table-striped">
								<tr>
									<td class="regbg" colspan="2">1. Identity</td>
								</tr>
								<tr>
									<td>
											<div class="form-group form-inline">
												<label class="col-sm-5">Name</label>
												<div class="col-sm-7">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<input  name="full_name" placeholder="Full Name" class="form-control"   type="text" required="required">
														<input name="membertype"  type="hidden" value="2">
													</div>
												</div>
											</div>
										</td>
										<td>
											<div class="form-group form-inline">
												<label class="col-sm-5">Photo</label>
												<div class="col-sm-5">
													<div class="input-group picture">
														<span class="input-group-addon"><i class="fa fa-camera"></i></span>
														<input type="file" name="fileToUpload" id="fileToUpload" class="form-control"   required="required">
													</div>
												</div>
											</div>
										</td>
								</tr>
								<tr>
									<td>
										<div class="form-group form-inline">
											<label class="col-md-5">Date of Birth</label>
											<div class="col-md-7">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input class="form-control " id="datepicker" name="dob" placeholder="YYYY-MM-DD" type="text"/>

												</div>
											</div>
										</div>
									</td>
									<td>
										<div class="form-group form-inline">
											<label class="col-md-5">Blood Group</label>
											<div class="col-md-7">
												<div class="input-group picture">
													<span class="input-group-addon"><i class="fa fa-tint"></i></span>
													<select name="blood" class="form-control selectpicker myselect" required="required">
														<option value="A+">A Positive (A+)</option>
														<option value="A-">A Negative (A-)</option>
														<option value="B+">B Positive (B+)</option>
														<option value="B-">B Negative (B-)</option>
														<option value="AB+">AB Positive(AB+)</option>
														<option value="AB-">AB Negative (AB-)</option>
														<option value="O+">O Positive (O+)</option>
														<option value="O-">O Negative (O-)</option>
													</select>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="regbg" colspan="2">2. Profession</td>
								</tr>
								<tr>
									<td>
										 <div class="form-group form-inline">
											  <label class="col-md-5">Institution</label>
											  <div class="col-md-7">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-building"></i></span>
														<input  name="institution" placeholder="Company/Institution Name" class="form-control"  type="text" required="required">
													</div>
											  </div>
										  </div>
									</td>
									  <td>
										   <div class="form-group form-inline">
											  <label class="col-md-5">Designation</label>
											  <div class="col-md-7">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-drivers-license-o"></i></span>
														<input  name="designation" placeholder="Designation" class="form-control"  type="text" required="required">
													</div>
											  </div>
										  </div>
									  </td>
								</tr>
								<tr>
									<td class="regbg" colspan="2">3. Address</td>
								</tr>
								<tr>
									<td>
										<div class="form-group form-inline">
											<label class="col-md-5 control-label">Present</label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="  fa fa-map-signs"></i></span>
														<textarea rows="1" class="form-control" cols="6" name="present_address" required="required"></textarea>
													</div>
												</div>
										</div>
									</td>
									  <td>
										  <div class="form-group form-inline">
											<label class="col-md-5">Permanent</label>
												<div class="col-md-7">
													<div class="input-group">
														<span class="input-group-addon"><i class="  fa fa-map-signs"></i></span>
														<textarea rows="1" class="form-control" cols="6" name="permanent_address" required="required"></textarea>
													</div>
												</div>
										  </div>
									  </td>
								</tr>
								<tr>
									<td class="regbg" colspan="2">4. Institute Identity</td>
								</tr>
								<tr>
									<td>
										<div class="form-group form-inline">
											<label class="col-md-5 control-label">Batch</label>
											  <div class="col-md-7 selectContainer">
												  <div class="input-group">
													  <span class="input-group-addon"><i class="fa fa-list"></i></span>


													  <select name="batch" class="form-control selectpicker myselect" required="required">
														<option value="">Select Batch</option>
														  <?php
														  function ordinal($number) {
															  $ends = array('th','st','nd','rd','th','th','th','th','th','th');
															  if ((($number % 100) >= 11) && (($number%100) <= 13))
																  return $number. 'th';
															  else
																  return $number. $ends[$number % 10];
														  }
														  //Example Usage
														  for($i=1; $i<=45;$i++){
															  echo "<option value=".ordinal($i).">".ordinal($i)."</option>";
														  }

														  ?>
													  </select>
												</div>
											  </div>
										</div>
									</td>
									<td>
										<div class="form-group form-inline">
											<label class="col-md-5">ID/Reg No.</label>
											<div class="col-md-7">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-key"></i></span>
													<input name="id_no" placeholder="(***)" class="form-control" type="text">
												</div>
											</div>
										</div>
									</td>
								  </tr>
								  <tr>
									<td class="regbg" colspan="2">5. Degrees Received</td>
								  </tr>
								  <tr>
									  <td>
										<table>
										  <tr>
											<td>
											   <div class="checkbox">
													<label class="col-md-12">
													  <input type="checkbox" id="honours" name="honours" value="yes" required="required">Honours
													</label>
												</div>
											</td>
											<td>
												<select id="honours_year" name="honours_year" class="col-md-12 form-control selectpicker myselect">
														<option value="">Select Year</option>
													<?php

													for($hy=1966; $hy<=2018;$hy++){
														echo "<option value=\"$hy\">".$hy."</option>";
													}


													?>
												</select>
											</td>
										  </tr>
										  <tr>
											<td>
											   <div class="checkbox">
													<label class="col-md-12">
													  <input type="checkbox" id="ma" name="ma" value="yes">MA
													</label>
												</div>
											</td>
											<td>
												<select id="ma_year" name="ma_year" class="col-md-12 form-control selectpicker myselect">
														<option value="">Select Year</option>
													<?php

													for($hy=1966; $hy<=2018;$hy++){
														echo "<option value=\"$hy\">".$hy."</option>";
													}


													?>
												</select>
											</td>
										  </tr>
										 </table>
									  </td>
									  <td>
										<table>
											<tr>
											<td>
											   <div class="checkbox">
													<label class="col-md-12">
													  <input type="checkbox" id="mphil" name="mphil" value="yes">MPhil
													</label>
												</div>
											</td>
											<td>
												<select id="mphil_year" name="mphil_year" class="col-md-12 form-control selectpicker myselect">
														<option value="">Select Year</option>
													<?php

													for($hy=1966; $hy<=2018;$hy++){
														echo "<option value=\"$hy\">".$hy."</option>";
													}


													?>
												</select>
											</td>
										  </tr>
										  <tr>
												<td>
												   <div class="checkbox">
														<label class="col-md-12">
														  <input type="checkbox" id="phd" name="phd" value="yes">PhD
														</label>
													</div>
												</td>
											<td>
												<select id="phd_year" name="phd_year" class="col-md-12 form-control selectpicker myselect">
														<option value="0">Select Year</option>
													<?php

													for($hy=1966; $hy<=2018;$hy++){
														echo "<option value=\"$hy\">".$hy."</option>";
													}


													?>
												</select>
											</td>
										  </tr>
										 </table>
									  </td>
								  </tr>
								  <tr>
									<td class="regbg" colspan="2">6. Contact</td>
								  </tr>
								  <tr>
										<td>
										  <div class="form-group form-inline">
											<label class="col-md-5">Mobile</label>
											  <div class="col-md-7">
											  <div class="input-group">
												  <span class="input-group-addon"><i class="fa fa-phone"></i></span>
											<input name="mobile_no" min="11" maxlength="11" placeholder="01714130077" class="form-control" type="text" required="required">
											  </div>
											</div>
										  </div>
										</td>
										<td>
										  <div class="form-group form-inline">
											<label class="col-md-5">E-Mail</label>
											  <div class="col-md-7">
											  <div class="input-group">
												  <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
											<input name="email" placeholder="E-Mail Address" class="form-control"  type="text"  required="required">
											  </div>
											</div>
										  </div>
										</td>
								  </tr>
								  <tr>
									<td id="nop" class="regbg" colspan="2">8. Number of participants</td>
								  </tr>
								  <tr>
									  <td>
										  <table align="center">
											  <tr>
												<th>Category</th>
												<th>Subscription</th>
											  </tr>
											  <tr>
												<td>
													<div class="checkbox">
													  <label>
														<input type="checkbox" id="self" class="tot_amount" name="self"  value="2000" required="required">Self
													  </label>
													</div>
												</td>
												<td>
												  <div class="checkbox">
													  <label>
													   Tk 2000
													  </label>
													</div>
												</td>
											  </tr>
											  <tr>
												  <td>
													  <div class="checkbox">
														  <label><input type="checkbox"  id="spouse" value="yes"></label> Spouse
													  </div>
													   <div class="radio">
														<label>
														  <input type="radio"  class="tot_amount" name="spouse" id="spouse_1" value="2000">Alumnus
														</label>
													  </div>
													 <div class="radio">
														<label>
														  <input type="radio" class="tot_amount" name="spouse" id="spouse_2" value="1500">Non-alumnus
														</label>
													  </div>
												  </td>
												  <td>
													<div class="checkbox">
													  <p>&nbsp;</p>
													  <label>
														Tk 2000
													  </label>
													</div>
													 <div class="checkbox">
													  <label>
														Tk 1500
													  </label>
													</div>
												  </td>
											  </tr>
										  </table>
									  </td>
									  <td>
										  <table>
											  <tr>
												<th>Category</th>
												<th>Subscription</th>
											  </tr>
											  <tr>
												<td>
													<div class="checkbox">
														  <input type="checkbox"    id="guest" value="yes">Guest:
														  &nbsp;&nbsp;<input id="guest_1" type="radio" class="tot_amount" name="guest"  value="1500">&nbsp;One
														  &nbsp;&nbsp;<input id="guest_2" type="radio" class="tot_amount" name="guest"  value="3000">&nbsp;Two
														  &nbsp;&nbsp;<input id="guest_3" type="radio" class="tot_amount" name="guest"  value="4500">&nbsp;Three


													</div>
												</td>
												<td>
												  <div class="checkbox">
													  <label>
														Tk 1500
													  </label>
													</div>
												</td>
											  </tr>
											  <tr>
												<td>
													<div class="checkbox">
													  <label>
														<input type="checkbox"  id="child" value="yes">Child (Above 5 Years)
														  &nbsp;&nbsp;<input id="child_1" type="radio" class="tot_amount" name="child"  value="1000">&nbsp;One
														  &nbsp;&nbsp;<input id="child_2" type="radio" class="tot_amount" name="child"  value="2000">&nbsp;Two
														  &nbsp;&nbsp;<input id="child_3" type="radio" class="tot_amount" name="child"  value="3000">&nbsp;Three
													  </label>
													</div>
												</td>
												<td>
												  <div class="checkbox">
													  <label>
														Tk 1000
													  </label>
													</div>
												</td>
											  </tr>
											  <tr>
												<td>
													<div class="checkbox">
													  <label>
														<input type="checkbox"   id="infant" value="yes">Infant (3-5 Years)
														  &nbsp;&nbsp;<input id="infant_1" type="radio" class="tot_amount" name="infant"  value="500">&nbsp;One
														  &nbsp;&nbsp;<input id="infant_2" type="radio" class="tot_amount" name="infant"  value="1000">&nbsp;Two
														  &nbsp;&nbsp;<input id="infant_3" type="radio" class="tot_amount" name="infant"  value="1500">&nbsp;Three
													  </label>
													</div>
												</td>
												<td>
												  <div class="checkbox">
													  <label>
														Tk 500
													  </label>
													</div>
												</td>
											  </tr>
											  <tr>
												<td>
													<div class="checkbox">
													  <label>
														<input type="checkbox"  class="tot_amount" name="baby" value="0">Baby (0-3 Years)

													  </label>
													</div>
												</td>
												<td>
												  <div class="checkbox">
													  <label>
														Tk 00
													  </label>
													</div>
												</td>
											  </tr>
										  </table>
									  </td>
								  </tr>
								<tr>
									<td class="text-right form-inline" colspan="2">
									   <div class="col-sm-6"> Total Subscription Fee: </div>
										<div class="col-sm-5">
											<input readonly id="total1" name="total" placeholder="" class="form-control" style="color:red; width:  font-size: 20px;" type="text" required="required">
										</div>
										<div class="col-sm-1"> Tk.</div>
									</td>
								</tr>
								  <tr>
									<td class="regbg">9. Food Choice</td>
									<td class="regbg">10. Mode Of Payment</td>
								  </tr>
								  <tr>
									<td>
										<div class="radio">
										  <label>
											<input type="radio" name="food" id="food" value="Beef" required="required">Beef
										  </label>
										</div>
										<div class="radio">
										  <label>
											<input type="radio" name="food" id="food" value="Non-Beef" required="required">Non-Beef
										  </label>
										</div>
									</td>
									<td>
										<table>
										  <tr>
											<td>
												<div class="radio">
													<label>
														<input type="radio" name="Payment_mode" id="optionsRadios1" value="Cash" checked>Cash
													</label>
												</div>
												<div class="radio">
													<label>
													  <input type="radio" name="Payment_mode" id="optionsRadios1" value="Bank" >Bank
													</label>
												  </div>
												  <div class="radio">
													<label>
													  <input type="radio" name="Payment_mode" id="optionsRadios1" value="Bkash">Bkash
													</label>
												  </div>
												  <div class="radio">
													<label>
													  <input type="radio" name="Payment_mode" id="optionsRadios1" value="Rocket">Rocket
													</label>
												  </div>

											</td>
											<td>
												<div class="form-group form-inline">

													<div class="col-md-12 inputGroupContainer">
													  <div class="input-group">
													  <span class="input-group-addon"><i class="fa fa-user"></i></span>
													  <input  name="payees_name" placeholder="Payee's Name" class="form-control"  type="text" required="required">
														</div>
													</div>
												</div>
												<div class="form-group form-inline">
													<div class="col-md-12 inputGroupContainer">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-key"></i></span>
															<input name="transaction_no" placeholder="Transaction Number" class="form-control" type="text" >
														</div>
													</div>
												</div>
											</td>
										  </tr>

										</table>
									</td>
								  </tr>
								<tr>
									<td class="regbg" colspan="2">10. Note:( <a class="text-danger">Important! Please provide the information required below. </a>) </td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<div class="form-group">

											<div class="col-md-12">
												<div class="">
													<textarea rows="1" placeholder="i.e. :Payment Receiver's Name, Bank Name, Branch Name etc." cols="6" name="note" class="form-control" required="required"></textarea>
												</div>
											</div>
										</div>
									</td>
								  </tr>
								  <tr>
									<td colspan="2">
									  <table>
										<h5  class="text-center"><strong>Payment Details</strong></h5>
										<tr>
										  <td class="note">
												 <b>Bank</b> : Through a demand draft favoring <b>"The Chittagong University English Alumni Association"</b> drawn on <b>Agrani Bank</b>, Chittagong University branch, Chittagong. To deposit directly - Account Title:<b>"The Chittagong University English Alumni Association"</b> Account No:<b>0200002102486 </b>.
												 <b>Bkash</b> (Personal) : 01822264956/01924441878 ;
												 <b>Rocket</b>(Personal) : 01819614810.

										  </td>
										</tr>
									  </table>
									</td>
								  </tr>
							</table>



							   <div align="center" class=" text-center">
								  <div class="text-danger text-center"> Please enter valid payment details for successful registration. </div>
								   <br>
								  <input type="submit" class="btn btn-primary" name="submit" value="Submit">
								</div>


							  </div>
						</fieldset>
					  </form>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
	
<?php
	include('footer.php');
?>